<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

/* Výpis osob */
$app->get('/persons', function (Request $request, Response $response, $args) {
    // V promene stmt je uloženy vysledek dotazu jako objekt databazoveho konektoru
    $stmt = $this->db->query('SELECT * FROM person ORDER BY first_name');

    //Je potreba prevest na datovou strukturu php

    $tplVars['osoby'] = $stmt->fetchAll();


    return $this->view->render($response, 'person.latte', $tplVars);
})->setName('persons');

/* Zobraz formular pro pridani nove osoby */
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first name' => '',
        'last name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street' => '',
        'street number' => '',
        'zip' => ''
    ];


    return $this->view->render($response, 'newPerson.latte', $tplVars);

})->setName('newPerson');


/* Odeslani formulare a spracovani dat */

$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender)
									VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");

            $stmt->bindValue(":nickname", $formData['nickname']);
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":last_name", $formData['last_name']);
            $stmt->bindValue(":id_location", null);
            $stmt->bindValue(":gender", empty($formData['gender']) ?  null : $formData['gender']);
            $stmt->bindValue(":height", !empty($formData['height']) ? $formData['height'] : null);
            $stmt->bindValue(":birth_day", !empty($formData['birth_day']) ? $formData['birth_day'] : null);

            $stmt->execute();
            $tplVars['message'] = 'Person succesfully inserted';
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error occured';
            $this->logger->error($e->getMessage());

        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newPerson.latte', $tplVars);

});


/* Editace uzivatele */
$app->get('/persons/update', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id person is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location) WHERE id_person = :id_person");
        $stmt->bindValue(":id_person", $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('person not found');
        } else {
            return $this->view->render($response, 'updatePerson.latte', $tplVars);
        }
    }

})->setName('persons_update');


